package main

import (
	"context"
	"fmt"
	"sync"
	"time"

	"gitee.com/cabel666/dumpackets/config"
	"gitee.com/cabel666/dumpackets/dfile"
	"gitee.com/cabel666/dumpackets/logg"
	"gitee.com/cabel666/dumpackets/ppackets"
	"github.com/google/gopacket"
	"github.com/google/gopacket/pcap"
)

var (
	ctx    context.Context
	cancel context.CancelFunc
)

func main() {
	flag := &ppackets.Single{Flag: false}
	wg := &sync.WaitGroup{}
	if config.Config.Routine.MonitorRoutine {
		go ppackets.PrintG()
	}
	handle, err := pcap.OpenLive(config.Config.Server.DeviceName, config.Config.Server.SnapLen,
		config.Config.Server.Promisc, config.Config.Server.Timeout)
	if err != nil {
		logg.Elogger.Err(err).Msg("监听流量失败")
	}
	defer handle.Close()
	err = handle.SetBPFFilter(config.Config.Server.BpfFilter)
	if err != nil {
		logg.Elogger.Fatal()
	}

	ctx, cancel = context.WithCancel(context.Background())
	pkts := gopacket.NewPacketSource(handle, handle.LinkType())
	wg.Add(1)
	go ppackets.PollP(ctx, wg, pkts, flag)
	ticker := time.NewTicker(time.Second * 10)
	defer ticker.Stop()
	for range ticker.C {
		if flag.Flag && dfile.CheckF() {
			flag.Flag = false
			cancel()
			wg.Wait()
			fmt.Println("旧的协程结束了, 开始新的G")
			time.Sleep(time.Second * 3)
			ctx, cancel = context.WithCancel(context.Background())
			wg.Add(1)
			go ppackets.PollP(ctx, wg, pkts, flag)
		}
	}
}
