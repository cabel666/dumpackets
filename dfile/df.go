package dfile

import (
	"context"
	"fmt"
	"os"
	"sync"
	"time"

	"gitee.com/cabel666/dumpackets/logg"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

func OpenF() *os.File {
	file, err := os.OpenFile("logs/dump.log", os.O_APPEND|os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		logg.Elogger.Err(err)
	}
	return file
}

func ChangeF() {
	t := time.Now()
	fileTime := t.Format("200601021504")
	fileName := "logs/dump-" + fileTime + ".log"
	err := os.Rename("logs/dump.log", fileName)
	if err != nil {
		logg.Elogger.Err(err)
	}
}

func WriteF(ctx context.Context, wg *sync.WaitGroup, file *os.File, udpLayer gopacket.Layer, p gopacket.Packet, ch <-chan struct{}) {
	defer wg.Done()
	<-ch
	ipLayer := p.Layer(layers.LayerTypeIPv4)
	if ipLayer != nil {
		udpPacket, _ := udpLayer.(*layers.UDP)
		ip, _ := ipLayer.(*layers.IPv4)
		writeString := fmt.Sprintf("%v, %d, %v\n", time.Now().Format("2006-01-02 15:04.000000"), udpPacket.Length, ip.SrcIP)
		file.WriteString(writeString)
	}
}

func CheckF() bool {
	previousModTime := time.Now()
	time.Sleep(time.Second * 5)
	fileInfo, err := os.Stat("logs/dump.log")
	if err != nil {
		logg.Elogger.Err(err)
	}
	if fileInfo.ModTime().After(previousModTime) {
		return false
	} else {
		return true
	}
}
