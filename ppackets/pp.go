package ppackets

import (
	"bufio"
	"context"
	"fmt"
	"os"
	"runtime"
	"sync"
	"time"

	"gitee.com/cabel666/dumpackets/config"
	"gitee.com/cabel666/dumpackets/dfile"
	"gitee.com/cabel666/dumpackets/logg"
	"github.com/google/gopacket"
	"github.com/google/gopacket/layers"
)

type Single struct {
	Flag bool
}

func PollP(ctx context.Context, wg *sync.WaitGroup, pkts *gopacket.PacketSource, sig *Single) {
	defer wg.Done()
	// 控制协程数量
	ch := make(chan struct{}, 512)
	file := dfile.OpenF()
	defer file.Close()
	for {
		select {
		case <-ctx.Done():
			sumP(file)
			dfile.ChangeF()
			return
		// 调用Packets方法，会开启新的goroutine
		case p, ok := <-pkts.Packets():
			if !ok {
				return
			}
			udpLayer := p.Layer(layers.LayerTypeUDP)
			if udpLayer != nil {
				ch <- struct{}{}
				wg.Add(1)
				go dfile.WriteF(ctx, wg, file, udpLayer, p, ch)
				sig.Flag = true
			}
		}
	}
}

func sumP(wf *os.File) {
	lineCount := 0
	file, err := os.Open(config.Config.Log.InfoLog)
	if err != nil {
		logg.Elogger.Err(err)
	}
	defer file.Close()
	scan := bufio.NewScanner(file)
	for scan.Scan() {
		lineCount++
	}
	writeString := fmt.Sprintf("本次共捕获到%d个UDP包", lineCount)
	wf.WriteString(writeString)
}

func PrintG() {
	ticker := time.NewTicker(config.Config.Routine.CheckInterval)
	defer ticker.Stop()
	for range ticker.C {
		fmt.Printf("当前goroutine数量: %d\n", runtime.NumGoroutine())
	}
}
